'use strict';

var loopback = require('loopback');
var _ = require('lodash');
var path = require('path');
var boot = require('loopback-boot');
var cookieParser = require('cookie-parser');
var session = require('express-session');

var app = module.exports = loopback();

var PassportConfigurator = require('loopback-component-passport').PassportConfigurator;
var passportConfigurator = new PassportConfigurator(app);

var bodyParser = require('body-parser');
var flash      = require('express-flash');

var config = {};
try {
  config = {
    provider: 'facebook',
    module: 'passport-facebook',
    profileFields: ['gender', 'link', 'locale', 'name', 'timezone',
      'verified', 'email', 'updated_time'],
    clientID: '752666325266476',
    clientSecret: 'cedd4786ac448f0f0a33235432e3abc5',
    callbackURL: '/auth/facebook/callback',
    authPath: '/auth/facebook',
    callbackPath: '/auth/facebook/callback',
    successRedirect: 'http://localhost:8100/',
    failureRedirect: 'http://localhost:8100/login',
    scope: ['email'],
    failureFlash: true,
    profileToUser: function(provider, profile, options) {
      return {
        name: profile.displayName,
        password: 'eventpie123',
        usingDefaultPassword: true,
        avatar: _.get(profile, 'photos[0].value'),
        email: _.get(profile, 'emails[0].value'),
      };
    },
  };
} catch (err) {
  console.trace(err);
  process.exit(1); // fatal
}

boot(app, __dirname);

// to support JSON-encoded bodies
app.middleware('parse', bodyParser.json());
// to support URL-encoded bodies
app.middleware('parse', bodyParser.urlencoded({
  extended: true,
}));

// The access token is only available after boot
app.middleware('auth', loopback.token({
  model: app.models.accessToken,
}));

// app.middleware('session:before', cookieParser(app.get('cookieSecret')));
app.middleware('session', session({
  secret: 'kitty',
  saveUninitialized: true,
  resave: true,
}));
passportConfigurator.init();

// We need flash messages to see passport errors
app.use(flash());

// Set up related models
passportConfigurator.setupModels({
  userModel: app.models.user,
  userIdentityModel: app.models.userIdentity,
  userCredentialModel: app.models.userCredential,
});

 // Configure passport strategies for third party auth providers
// for (var s in config) {
//   var c = config[s];
//   c.session = c.session !== false;
// }
passportConfigurator.configureProvider('facebook-login', config);

var ensureLoggedIn = require('connect-ensure-login').ensureLoggedIn;

app.get('/', function(req, res, next) {
  res.cookie('cookie1', 'This is my first cookie',
    {signed: true, maxAge: 1000 * 60 * 60 * 24 * 7, httpOnly: true}
  );
  res.end('Cookie has been set');
});

app.start = function() {
  // start the web server
  return app.listen(function() {
    app.emit('started');
    var baseUrl = app.get('url').replace(/\/$/, '');
    console.log('Web server listening at: %s', baseUrl);
    if (app.get('loopback-component-explorer')) {
      var explorerPath = app.get('loopback-component-explorer').mountPath;
      console.log('Browse your REST API at %s%s', baseUrl, explorerPath);
    }
  });
};

// start the server if `$ node server.js`
if (require.main === module) {
  app.start();
}
